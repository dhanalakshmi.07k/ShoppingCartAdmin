var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    categoriesDetailsModel = mongoose.model('Categories'),
    Categories = mongoose.model('Categories'),
    subCategoriesDetailsModel = mongoose.model('SubCategories');
    module.exports = function (app){
        app.use('/', router);
    };

router.post('/category', function(req, res, next) {

    var newcategoriesDetails = new categoriesDetailsModel(req.body);
    categoriesDetailsModel.count(function(err,categorycount){
            if(categorycount!=0)
            {
      newcategoriesDetails.categoryId=categorycount+1;
    newcategoriesDetails.save(function(err,result) {
        if (err){
            console.log('Error in Saving user: '+err);
        }
        res.send(result);
    });

}
else if(categorycount==0)
{
newcategoriesDetails.categoryId=1;
    newcategoriesDetails.save(function(err,result) {
        if (err){
            console.log('Error in Saving user: '+err);
        }
        res.send(result);
    });

}
});
});



router.get('/allCategory', function(req, res, next) {
 categoriesDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/allCategoryCount',function(req,res,next){
categoriesDetailsModel.count({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
        result:result
            res.send({count:result});
        }

    }).skip(req.query.offset).limit(req.query.limit);

})




router.get('/categoryByMongoId/:categoryMongoId',function(req,res,next){

categoriesDetailsModel.find({"_id":req.params.categoryMongoId},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

})









router.post('/editcategoryBymongoId', function(req, res, next) {
console.log(req.body);
console.log(req.body._id);
 categoriesDetailsModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},function(err,result){
        if(err){
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.delete('/categoryBymongoId/:categoryMongoid',function(req, res, next){
categoriesDetailsModel.remove({"_id":req.params.categoryMongoid},function(err,result)
{
if(err)
{
 console.log(err.stack)
}
else
{
 res.send(result)
}

});
});


//router.get('/categorykey',function(req,res,next){
//categoriesDetailsModel.findOne(function(err,result){
//                                                            if(err)
//                                                                  {
//                                                                  console.log(err.stack)
//                                                                }
//                                                                  else
//                                                                 {
//                                                                 console.log(result);
//                                                                 res.send(result);
//
//                                                                 }
//
//
//                                                               })
//
//})



router.get('/categoryByName/:categoryName', function(req, res, next) {
 categoriesDetailsModel.find({"categoryName":req.params.categoryName},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})





router.route('/SubCatagorieListByCategory')
    .get(function(req,res){
        Categories.aggregate([
            {$lookup:
                {
                    from:"SubCategories",
                    localField:"categoryId",
                    foreignField:"categoryId",
                    as: "SubCategories"
                }
            }
        ],function (err, result) {
                if (err) {
                    res.send(err)
                }
                else{
                    res.send(result)
                }

            })


    });

    router.get('/categoryId',function(req,res,next){


    categoriesDetailsModel.find({},{categoryId:1},function(err,result){
                        if(err)
                            {
                             console.log(err.stack)
                            }
                         else
                          {
                             console.log(result);
                             res.send(result);

                            }


                           })

    })




     router.get('/allCategoryNameAndCategoryId',function(req,res,next){


        categoriesDetailsModel.find({},{categoryName:1,categoryId:1},function(err,result){
                            if(err)
                                {
                                 console.log(err.stack)
                                }
                             else
                              {
                                 console.log(result);
                                 res.send(result);

                                }


                               })

        })




