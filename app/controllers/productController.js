var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    fs=require('fs'),
    productDetailsModel = mongoose.model('Product'),
     Categoriesmodel = mongoose.model('Categories'),
        subCategoriesDetailsModel = mongoose.model('SubCategories');

    module.exports = function (app){
        app.use('/', router);
    };

router.post('/product', function(req, res, next) {

    var newproductDetailsModel = new productDetailsModel(req.body);
    productDetailsModel.count(function(err,productCount){
    if(productCount!=0)
    {

     console.log(productCount);
     newproductDetailsModel.productId=productCount+1;

    newproductDetailsModel.save(function(err,result) {
        if (err){
            console.log('Error in Saving user: '+err);
        }
        console.log(result);
        res.send(result);
    });
    }
    else if(productCount==0)
    {
     console.log(productCount);
         newproductDetailsModel.productId=productCount+1;

        newproductDetailsModel.save(function(err,result) {
            if (err){
                console.log('Error in Saving user: '+err);
            }
            res.send(result);
        });
    }
});
});



router.get('/allProduct', function(req, res, next) {
 productDetailsModel.find({},{productMoreImages:0},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
//        for(var i=0;result.length;i++)
//        {
//        console.log([i]);
//      var data=fs.readFileSync( result[i].productImage);
//              var data=new Buffer(result[i]);
//         var consss=data.toString('base64');
//console.log("hhhhhhh"+consss);
//        }
//        console.log(result);
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/allProductCount',function(req,res,next){
productDetailsModel.count({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
        result:result
            res.send({count:result});
        }

    }).skip(req.query.offset).limit(req.query.limit);

})



router.get('/productByMongoId/:productMongoId',function(req,res,next){

productDetailsModel.find({"_id":req.params.productMongoId},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

})







router.post('/editproductBymongoId', function(req, res, next) {
console.log(req.body);
console.log(req.body._id);
 productDetailsModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},function(err,result){
        if(err){
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.delete('/productBymongoId/:productMongoid',function(req, res, next){
productDetailsModel.remove({"_id":req.params.productMongoid},function(err,result)
{
if(err)
{
 console.log(err.stack)
}
else
{
 res.send(result)
}

});
});


//router.get('/productName',function(req,res,next){
//
//
//productDetailsModel.findOne(function(err,result){
//                    if(err)
//                        {
//                         console.log(err.stack)
//                        }
//                     else
//                      {
//                         console.log(result);
//                         res.send(result);
//
//                        }
//
//
//                       })
//
//})

router.get('/productByName/:productName',function(req,res,next){

console.log(req.params.productName);
productDetailsModel.find({"productName":req.params.productName},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

})



router.route('/productListByCategory')
    .get(function(req,res){
        Categories.aggregate([
            {$lookup:
                {
                    from:"Product",
                    localField: "categoryId",
                    foreignField:"categoryId",
                    as: "ProductUnderCategory"
                }
            }
        ],function (err, result) {
                if (err) {
                    res.send(err)
                }
                else{
                    res.send(result)
                }

            })


    });



router.route('/productListBySubCategory')
    .get(function(req,res){
        subCategoriesDetailsModel.aggregate([
            {$lookup:
                {
                    from:"Product",
                    localField:"subCategoryid",
                    foreignField:"subCategoryid",
                    as: "productunderSubCategories"
                }
            }
        ],function (err, result) {
                if (err) {
                    res.send(err)
                }
                else{
                console.log(result)
                    res.send(result)
                }

            })


    });



router.route('/productListBySubCatid/:subcatid')

    .get(function(req,res){
    console.log(req.params.subcatid);
//    var s=req.params.subcatid;
        subCategoriesDetailsModel.aggregate([

//          {
//              $filter:
//              {
//                input: "$Product",
//                as: "pet",
//                cond: { $eq: [ "$$Product.subCategoryid",req.params.subcatid ] }
//              }
//            } ,
          {
                $match:{
                    "subCategoryid": {$eq: Number(req.params.subcatid) }
                }
                },
            {$lookup:
                {
                    from:"productDetailsModel",
                    localField:"subCategoryid",
                    foreignField:"subCategoryid",
                    as: "SubCategorieslist"
                }
            }
//    {
//        preserveNullAndEmptyArrays : true,
//        path : "$SubCategorieslist"
//    }
//




        ],function (err, result) {
                if (err) {
                    res.send(err)
                }
                else{
                console.log(result)
                    res.send(result)
                }

            })


    })








//router.route('/productListByCategoryAndSubCategory')
//    .get(function(req,res){
//        Categories.aggregate([
//            {$lookup:
//                {
//                    from:"SubCategories",
//                    localField:"categoryId",
//                    foreignField:"categoryId",
//                    as: "SubCategoriesbyCategory"
//                }
//            },
//
//
//            {
//              $unwind:{
//                     path:"$SubCategoriesbyCategory",
//                     "preserveNullAndEmptyArrays": true
//                    }
//               },
//
//
//// {$match: {$expr: {$eq: ["SubCategoriesbyCategory.categoryId", "categoryId"]}}},
//// {$match:{"SubCategoriesbyCategory.categoryId":{$exists:true},"categoryId":{$exists:true}}},
////                    {$project: {
////                        "SubCategoriesbyCategory.categoryId":1,
////                        "categoryId":1,
////                        "aCmp": {$cmp:["$SubCategoriesbyCategory.categoryId","$categoryId"]}
////                      }
////                    },
////                    {$match:{"aCmp":1}},
//
//
//
//
//
//              {$lookup:
//                   {
//                  from:"Product",
//                  localField:"SubCategoriesbyCategory.subCategoryid",
//                  foreignField:"subCategoryid",
//                  as:"productList"
//                   }
//                  },
//
////     {$project: {
////        // All your other fields here
////        cmp_value: {$cmp: ['$SubCategoriesbyCategory.categoryId', '$productList.categoryId']}
////    }},
////    {$match: {cmp_value:1}},
//
//                  {
//                $unwind:{
//                        path:"$productList",
//                        "preserveNullAndEmptyArrays": true
//                      }
//                    }],function (err, result) {
//                if (err) {
//                    res.send(err)
//                }
//                else{
//                console.log(result)
//                    res.send(result)
//                }
//
//            })
//
//
//    });
//
//
//


router.route('/productListByCategoryAndSubCategory')
    .get(function(req,res){
        Categoriesmodel.aggregate([
            {$lookup:
                {
                    from:"SubCategories",
                    localField:"categoryId",
                    foreignField:"categoryId",
                    as: "SubCategoriesbyCategory"
                }
            },
//
//
            {
              $unwind:{
                     path:"$SubCategoriesbyCategory",
                     "preserveNullAndEmptyArrays": true
                    }
               },


// {$match: {$expr: {$eq: ["SubCategoriesbyCategory.categoryId", "categoryId"]}}},
// {$match:{"SubCategoriesbyCategory.categoryId":{$exists:true},"categoryId":{$exists:true}}},
//                    {$project: {
//                        "SubCategoriesbyCategory.categoryId":1,
//                        "categoryId":1,
//                        "aCmp": {$cmp:["$SubCategoriesbyCategory.categoryId","$categoryId"]}
//                      }
//                    },
//                    {$match:{"aCmp":1}},





              {$lookup:
                   {
                  from:"Product",
                  localField:"SubCategoriesbyCategory.subCategoryid",
                  foreignField:"subCategoryid",
                  as:"productList"
                   }
                  },

//     {$project: {
//        // All your other fields here
//        cmp_value: {$cmp: ['$SubCategoriesbyCategory.categoryId', '$productList.categoryId']}
//    }},
//    {$match: {cmp_value:1}},

//                  {
//                $unwind:{
//                        path:"$productList",
//                        "preserveNullAndEmptyArrays": true
//                      }
//                    }
                    ],function (err, result) {
                if (err) {
                    res.send(err)
                }
                else{
                console.log(result)
                    res.send(result)
                }

            })


    });



//    router.route('/productListByCategoryAndSubCategory')
//        .get(function(req,res){
//            Categories.aggregate([
//                {$lookup:
//                    {
//                        from:"SubCategories",
//                        localField:"categoryId",
//                        foreignField:"categoryId",
//                        as: "SubCategoriesbyCategory"
//                    }
//                },
//
//
//                {
//                  $unwind:{
//                         path:"$SubCategoriesbyCategory",
//                         "preserveNullAndEmptyArrays": true
//                        }
//                   },
//
//
//    // {$match: {$expr: {$eq: ["SubCategoriesbyCategory.categoryId", "categoryId"]}}},
//    // {$match:{"SubCategoriesbyCategory.categoryId":{$exists:true},"categoryId":{$exists:true}}},
//    //                    {$project: {
//    //                        "SubCategoriesbyCategory.categoryId":1,
//    //                        "categoryId":1,
//    //                        "aCmp": {$cmp:["$SubCategoriesbyCategory.categoryId","$categoryId"]}
//    //                      }
//    //                    },
//    //                    {$match:{"aCmp":1}},
//
//
//
//
//
//                  {$lookup:
//                       {
//                      from:"Product",
//                      localField:"SubCategoriesbyCategory.subCategoryid",
//                      foreignField:"subCategoryid",
//                      as:"productList"
//                       }
//                      },
//
//    //     {$project: {
//    //        // All your other fields here
//    //        cmp_value: {$cmp: ['$SubCategoriesbyCategory.categoryId', '$productList.categoryId']}
//    //    }},
//    //    {$match: {cmp_value:1}},
//
//                      {
//                    $unwind:{
//                            path:"$productList",
//                            "preserveNullAndEmptyArrays": true
//                          }
//                        }],function (err, result) {
//                    if (err) {
//                        res.send(err)
//                    }
//                    else{
//                    console.log(result)
//                        res.send(result)
//                    }
//
//                })
//
//
//        });






router.get('/productId',function(req,res,next){


productDetailsModel.find({},{productId:1},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }



          })

})




//
//router.post('/uploadCompanyLogo', function (req, res) {
//console.log(req.body);
//    var fromDetails={}
//    var form = new formidable.IncomingForm();
//    form.uploadDir = __dirname + "/data";
////    console.log(form.uploadDir);
//    form.keepExtensions = true;
//    form.parse(req, function(err, fields, files) {
//        if (!err) {
//            fromDetails=files
//            console.log('File uploaded :' + files.file.path);
//            grid.mongo = mongoose.mongo;
//            var conn = mongoose.createConnection('mongodb://127.0.0.1:27017/testFile');
//
//            conn.once('open', function () {
//
//                var gfs = grid(conn.db);
//                var writestream = gfs.createWriteStream({
//                    filename: files.file.name
//                });
//                fs.createReadStream(files.file.path).pipe(writestream);
//            });
//        }
//    });
//
//    form.on('end', function() {
//        res.send(fromDetails.file.name);
//        console.log(fromDetails.file);
//    });
//
//});
//
//


router.post('/updateMoreImages/:mongoid/:filename',function(req,res,next){
productDetailsModel.findOneAndUpdate({_id:req.params.mongoid},{ $push:{productMoreImages:req.params.filename}},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }



      })

})



router.post('/getAllProductImages',function(req,res,next){
productDetailsModel.find({},{productId:1,subCategoryid:1,categoryId:1,productImage:1,productMoreImages:1},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }



      })

})




router.post('/getAllProductImagesbystring',function(req,res,next){
productDetailsModel.find({},{productId:1,subCategoryid:1,categoryId:1,productImage:1},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }



      })

})


router.get('/getAllProductImagesbystring/:productname',function(req,res,next){
productDetailsModel.find({productImage:req.params.productname},{productImage:1},function(err,result){
if(err)
{
console.log(err.stack)
}
else
{
console.log(result);
res.send(result);

}



})

})









