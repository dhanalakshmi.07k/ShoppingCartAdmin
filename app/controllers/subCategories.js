var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    subCategoriesDetailsModel = mongoose.model('SubCategories'),
     productDetailsModel = mongoose.model('Product');
    module.exports = function (app){
        app.use('/', router);
    };
router.post('/subCatagory', function(req, res, next) {

    var newsubCategoriesDetails = new subCategoriesDetailsModel(req.body);
   subCategoriesDetailsModel.count(function(err,subCategorycount){
        if(subCategorycount!=0)
        {
    newsubCategoriesDetails.subCategoryid=subCategorycount+1;
    newsubCategoriesDetails.save(function(err,result) {

        if (err){
            console.log('Error in Saving user: '+err);
        }
        res.send(result);
    });
}
else if(subCategorycount==0)
{
 newsubCategoriesDetails.subCategoryid=1;
newsubCategoriesDetails.save(function(err,result) {

         if (err){
             console.log('Error in Saving user: '+err);
         }
         res.send(result);
     });


}
});
});

router.get('/allSubCatagory', function(req, res, next) {
 subCategoriesDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/allSubCategoryCount',function(req,res,next){
subCategoriesDetailsModel.count({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
        result:result
            res.send({count:result});
        }

    }).skip(req.query.offset).limit(req.query.limit);

})






router.get('/subCategoryBymongoId/:subCategoryMongoid', function(req, res, next) {
console.log(req.body);
console.log(req.params.subCategoryMongoid);
 subCategoriesDetailsModel.find({"_id":req.params.subCategoryMongoid},function(err,result){
        if(err){
            console.log(err.stack)
        }else{
        console.log(result);
            res.send(result)
        }

    })

})


router.post('/editsubCategoryBymongoId', function(req, res, next) {
console.log(req.body);
console.log(req.body._id);
 subCategoriesDetailsModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},function(err,result){
        if(err){
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})




router.delete('/subCategoryBymongoId/:subCategoryMongoid',function(req, res, next){
subCategoriesDetailsModel.remove({"_id":req.params.subCategoryMongoid},function(err,result)
{
if(err)
{
 console.log(err.stack)
}
else
{
 res.send(result)
}

});
});




//router.get('/subCategoryName',function(req,res,next){
//
//
//subCategoriesDetailsModel.findOne(function(err,result){
//                    if(err)
//                        {
//                         console.log(err.stack)
//                        }
//                     else
//                      {
//                         console.log(result);
//                         res.send(result);
//
//                        }
//
//
//                       })
//
//})


router.get('/subCategoryByName/:subCategoryName',function(req,res,next){


subCategoriesDetailsModel.findOne({"subCategoryname":req.params.subCategoryName},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

})

router.get('/subCategoryId',function(req,res,next){


subCategoriesDetailsModel.find({},{subCategoryid:1},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

})


router.get('/subCategoryNameAndCategoryId',function(req,res,next){


subCategoriesDetailsModel.find({},{subCategoryname:1,subCategoryid:1},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

})