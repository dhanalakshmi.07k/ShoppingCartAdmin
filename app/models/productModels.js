var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var ShoppingSubCategorySchema = new mongoose.Schema(
  {
      productId:Number,
    productName:String,
    subCategoryname:String,
    subCategoryid:Number,
    categoryName:String,
    categoryId:Number,
    productPrice:Number,
    productOfferId:Number,
    productOfferName:String,
    productAvailableqty:Number,
    productSoldqty:Number,
    productImage:String,
    productShortdescription:String,
    productLongdescription:String,
    productMoreImages:Array

  },{collection: "Product"});
  mongoose.model('Product',ShoppingSubCategorySchema);
