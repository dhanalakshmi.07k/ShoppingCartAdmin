var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var ShoppingSubCategorySchema = new mongoose.Schema(
  {
     subCategoryid:Number,
    subCategoryname:String,
    subCategorydescription:String,
    categoryName:String,
    categoryId:Number


      },{collection: "SubCategories"});
  mongoose.model('SubCategories',ShoppingSubCategorySchema);
