var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var ShoppingOfferSchema = new mongoose.Schema(
  {
    productOfferName:String,
    productOfferId:Number,
    productOfferRate:Number
},{collection:"Offer"});
  mongoose.model('Offer',ShoppingOfferSchema);
