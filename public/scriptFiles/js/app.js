var shoppingAdminapp = angular.module('myAdmin', ['ui.router', 'angularUtils.directives.dirPagination','naif.base64']);


shoppingAdminapp.run(function(categoryServices,productServices,subcategoryServices){

function getCategoryConfig(){

        categoryServices.getNameForCategory().then(function (resultDetails) {
         console.log(resultDetails.data);
          categoryServices.setNameForCategory(resultDetails.data);
        }, function error(errResponse) {
            console.log("cannot get settings config")
        })

    }


 function getSubCategoryConfig(){

         subcategoryServices.getNameForSubCategory().then(function (resultDetails) {
         console.log(resultDetails.data);
           subcategoryServices.setNameForSubCategory(resultDetails.data);
         }, function error(errResponse) {
             console.log("cannot get settings config")
         })

     }
 function getProductConfig(){

              productServices.getNameForProduct().then(function (resultDetails) {
              console.log(resultDetails.data);
                productServices.setNameForProduct(resultDetails.data);
              }, function error(errResponse) {
                  console.log("cannot get settings config")
              })

          }


   function init(){
  getCategoryConfig();
  getSubCategoryConfig();
  getProductConfig();

  }
      init();
})





shoppingAdminapp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider

            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'shoppinAdmin/dashboard.html',
                controller:'dashboardCtrl'
            })

            .state('products', {
                url: '/products',
                templateUrl: 'shoppinAdmin/products.html',
                controller:'productController'
            })
            .state('subcategory', {
                url: '/subcategory',
                templateUrl: 'shoppinAdmin/subcategory.html',
                controller:'subCategorycontroller'
            })

             .state('category', {
                 url: '/category',
                 templateUrl:'shoppinAdmin/category.html',
                 controller:'categoryController'
                        })

                      .state('productbycatsubcat', {
                           url: '/productbycatsubcat',
                                templateUrl: 'shoppinAdmin/productbycatsubcat.html',
                                controller:'productController'
                           })

                            .state('catsubcat', {
                                                      url: '/catsubcat',
                                                           templateUrl: 'shoppinAdmin/categorysubcategory.html',
                                                           controller:'categoryController'
                                                      })

              .state('settings', {
                url: '/settings',
                templateUrl: 'shoppinAdmin/settings.html',
                controller:'companySettingcontroller'
            })
            .state('offers', {
            url:'/offers',
            templateUrl:'shoppinAdmin/offer.html',
            controller:'offerController'
            });
    });


 /*routerApp.contoller('MainCtrl',function MainCtrl() {});

       MainCtrl.$inject = ['$scope','MainFactory'];

       function MainCtrl($scope, MainFactory) {
              $scope.details = MainFactory.details;
              function init() {
              MainFactory.get();
              }

              init();

              $scope.detailsModel = {
                    "product_id":1,
                          "product_name":"Samsung ON Pro",
                          "product_cost":12000
                          };

              $scope.add = function () {
                    $scope.details.push($scope.detailsModel);
                    };

              $scope.delete = function (index) {
                    $scope.details.splice(index, 1);
              };

              $scope.edited = -1;
              $scope.editedModel = {
                    "product_id":0,
                    "product_name":"",
                    "product_cost":
                    };

              $scope.edit = function (index) {
                    $scope.edited = index;
                    };

              $scope.finishEdit = function (index) {
                    $scope.details[index] = $scope.editedModel;
                    $scope.edited = -1;
                    };


              };


*/



