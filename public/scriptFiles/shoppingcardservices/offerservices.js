shoppingAdminapp.factory('offerServices',function($http){
var setName;
  var postOfferdetails = function(offerdata)
  {
  console.log(offerdata);
          return $http.post('/offer',offerdata);
    }
    var getAllofferDetails=function()
    {
            return $http.get('/allOffer');
      }
      var editOfferbyMongoId=function(editdata)
      {
              return $http.post('/editOfferBymongoId',editdata);
        }
        var deleteOfferbyMongoId=function(mongoid){
                return $http.delete('/offerBymongoId/'+mongoid);
          }
      var getNameForOffer=function()
      {
       return $http.get('/offerkey');

      }
 var setNameForOffer=function(getName)
      {
      setName=getName;
      return setName;
      }
var getOfferDetailsByName=function(offerName)
{
return $http.get('/offerByName/'+offerName);
}

var getEditOfferDetailsByMongoId=function(mongoid)
{
return $http.get('/offerByMongoId/'+mongoid);
}

var getAllOfferId=function()
{
return $http.get('/getAllOfferIdAndName')
}



       return{
       postOfferdetails:postOfferdetails,
       getAllofferDetails:getAllofferDetails,
       editOfferbyMongoId:editOfferbyMongoId,
       deleteOfferbyMongoId:deleteOfferbyMongoId,
       getNameForOffer:getNameForOffer,
       getEditOfferDetailsByMongoId:getEditOfferDetailsByMongoId,
       getOfferDetailsByName:getOfferDetailsByName,
       getAllOfferId:getAllOfferId
      }
})