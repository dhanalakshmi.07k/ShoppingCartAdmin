shoppingAdminapp.factory('categoryServices',function($http){
var setName;

  var postCategorydetails = function(categorydata)
  {
          return $http.post('/category',categorydata);
    }
    var getAllcategoryDetails=function()
    {
            return $http.get('/allCategory');
      }
      var editCatbyMongoId=function(editdata)
      {
              return $http.post('/editcategoryBymongoId',editdata);
        }
        var deleteCatbyMongoId=function(mongoid){
                return $http.delete('/categoryBymongoId/'+mongoid);
          }
      var getNameForCategory=function()
      {
       return $http.get('/categorykey');

      }
 var setNameForCategory=function(getName)
      {
      setName=getName;
      return setName;
      }
var getCategoryDetailsByName=function(categoryName)
{
return $http.get('/categoryByName/'+categoryName);
}

var getEditCategoryDetailsByMongoId=function(mongoid)
{
return $http.get('/categoryByMongoId/'+mongoid);
}

var getAllCategoryID=function()
{
return $http.get('/categoryId');
}

var getAllCategoryName=function()
{
return $http.get('/allCategoryNameAndCategoryId');
}

var getSubCategoryByCategory=function()
{
return $http.get('/SubCatagorieListByCategory');
}
var getCategorycount=function()
{
return $http.get('/allCategoryCount');
}


       return{
       postCategorydetails:postCategorydetails,
       getAllcategoryDetails:getAllcategoryDetails,
       editCatbyMongoId:editCatbyMongoId,
       deleteCatbyMongoId:deleteCatbyMongoId,
       getNameForCategory:getNameForCategory,
       setNameForCategory:setNameForCategory,
       getCategoryDetailsByName:getCategoryDetailsByName,
       getEditCategoryDetailsByMongoId:getEditCategoryDetailsByMongoId,
       getAllCategoryID:getAllCategoryID,
       getAllCategoryName:getAllCategoryName,
       getSubCategoryByCategory:getSubCategoryByCategory,
       getCategorycount:getCategorycount
      }
})