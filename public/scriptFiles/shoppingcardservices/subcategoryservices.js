shoppingAdminapp.factory('subcategoryServices',function($http){

var setName={};
var categorynameandid;
  var postSubcategoryDetails = function(subcategorydata)
  {
   return $http.post('/subCatagory',subcategorydata);
    }

    var getAllsubCategorydetailslist = function()
    {
            return $http.get('/allSubCatagory');
      }

      var editSubcategoryBymongoid = function(editid)
      {
              return $http.get('/subCategoryBymongoId/'+editid);
        }
        var editAndUpdateSubcategoryBymongoid=function(editdata)
        {
        return $http.post('/editsubCategoryBymongoId',editdata);
        }


        var deleteSubcategoryBymongoid=function(mongoid)
        {
                return $http.delete('/subCategoryBymongoId/'+mongoid);
          }

var getNameForSubCategory=function()
      {
       return $http.get('/categorykey');

      }
 var setNameForSubCategory=function(getName)
      {
      setName=getName;
      return setName;
      }


var getSubCategoryDetailsByName=function(subCatName)
{
return $http.get('/subCategoryByName/'+subCatName);
}
var getAllSubCategoryID=function()
{
return $http.get('/subCategoryId');
}

var getAllSubCategoryName=function()
{
return $http.get('/subCategoryNameAndCategoryId');
}
var setCategoryNameAndId=function(categoryData)
{
console.log(categoryData);
 categorynameandid=categoryData;
}

var getCategoryNameAndId=function()
{
return categorynameandid;
}

var getSubCategoryCount=function()
{
return $http.get('/allSubCategoryCount');
}


       return{
       postSubcategoryDetails:postSubcategoryDetails,
       getAllsubCategorydetailslist:getAllsubCategorydetailslist,
       editSubcategoryBymongoid:editSubcategoryBymongoid,
       editAndUpdateSubcategoryBymongoid:editAndUpdateSubcategoryBymongoid,
       deleteSubcategoryBymongoid:deleteSubcategoryBymongoid,
       getNameForSubCategory:getNameForSubCategory,
       setNameForSubCategory:setNameForSubCategory,
       getSubCategoryDetailsByName:getSubCategoryDetailsByName,
       getAllSubCategoryID:getAllSubCategoryID,
       getAllSubCategoryName:getAllSubCategoryName,
       setCategoryNameAndId:setCategoryNameAndId,
       getCategoryNameAndId:getCategoryNameAndId,
       getSubCategoryCount:getSubCategoryCount


       }


})