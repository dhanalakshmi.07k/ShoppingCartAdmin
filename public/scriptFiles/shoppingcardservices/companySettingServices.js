shoppingAdminapp.factory('companySettingServices',function($http){
var setName;
var logonamedata;
var logodata;

  var postCompanySettingDetails = function(companysetting)
  {
         console.log(companysetting);
          return $http.post('/companysetting',companysetting);
    }
    var getCompanySettingDetails=function()
    {
            return $http.get('/getCompanySetting');
      }
      var editCompanySetting=function(editdata)
      {
              return $http.post('/updateCompanySetting',editdata);
        }
        var setLogoNameFileName=function(logoname)
        {
       logonamedata=logoname;
        }
        var getLogoNameFileName=function()
        {
        return logonamedata;
        }

        var setCompanyLogoImages=function(logo)
        {
//        console.log(logo);
        logodata=logo;
        }
        var getCompanyLogoImages=function()
        {
        return logodata;
        }



       return{
       postCompanySettingDetails:postCompanySettingDetails,
       getCompanySettingDetails:getCompanySettingDetails,
       editCompanySetting:editCompanySetting,
       setLogoNameFileName:setLogoNameFileName,
       getLogoNameFileName:getLogoNameFileName,
       setCompanyLogoImages:setCompanyLogoImages,
       getCompanyLogoImages:getCompanyLogoImages


      }
})