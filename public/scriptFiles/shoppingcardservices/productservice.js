shoppingAdminapp.factory('productServices',function($http){
var subCatDetails;
var offerdetails;
var productImages;
  var postProductdetails = function(productdata)
   {
   console.log(productdata);
          return $http.post('/product',productdata);
    }
    var getAllproductDetails = function()
     {
            return $http.get('/allProduct');
     }
      var editProcuctbyMongoId = function(editobj)
      {
      console.log(editobj);
              return $http.post('/editproductBymongoId',editobj);
       }
        var deleteProcuctbyMongoId= function(mongoid)
        {
                return $http.delete('/productBymongoId/'+mongoid);
         }

  var getNameForProduct=function()
      {
       return $http.get('/productName');
      }

 var setNameForProduct=function(getName)
      {
      if(getName){
      console.log(getName)
     return getName;
      }
}

var getProductDetailsByName=function(productName)
{
return $http.get('/productByName/'+productName);
}

var getEditProductDetailsByMongoId=function(mongoid)
{
return $http.get('/productByMongoId/'+mongoid);
}
var getProductByCategorySubCategory=function()
{
return $http.get('/productListByCategoryAndSubCategory')
}

var getAllProductID=function()
{
return $http.get('/productId');
}
var getAllProductBySubCatId=function(subcatid)
{
return $http.get('/productListBySubCatid/'+subcatid)
}

var getProductBySubCategory=function(){
return $http.get('/productListBySubCategory');
}
var setSubCategoryNameAndId=function(subcatDetails)
{
subCatDetails=subcatDetails;
}
var getSubCategoryNameAndId=function()
{
return subCatDetails;
}
var setofferNameAndId=function(offerName)
{
offerdetails=offerName;
}
var getofferNameAndId=function()
{
return offerdetails
}

var setProductImages=function(images){
productImages=images;
}

var getProductImage=function()
{
return productImages;
}

var updateMoreImages=function(mongoid,filename)
{
return $http.post('/updateMoreImages/'+mongoid+'/'+filename);
}

var productCount=function(){
return $http.get('/allProductCount');
}

       return {
       postProductdetails:postProductdetails,
       getAllproductDetails:getAllproductDetails,
       editProcuctbyMongoId:editProcuctbyMongoId,
       deleteProcuctbyMongoId:deleteProcuctbyMongoId,
       getNameForProduct:getNameForProduct,
       setNameForProduct:setNameForProduct,
       getProductDetailsByName:getProductDetailsByName,
       getEditProductDetailsByMongoId:getEditProductDetailsByMongoId,
       getProductByCategorySubCategory:getProductByCategorySubCategory,
       getAllProductID:getAllProductID,
       setSubCategoryNameAndId:setSubCategoryNameAndId,
       getSubCategoryNameAndId:getSubCategoryNameAndId,
       setofferNameAndId:setofferNameAndId,
       getofferNameAndId:getofferNameAndId,
       setProductImages:setProductImages,
       getProductImage:getProductImage,
       getProductBySubCategory:getProductBySubCategory,
       updateMoreImages:updateMoreImages,
       productCount:productCount


       }
})